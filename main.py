import nextcord
from config import token, ids

import logging
logging.basicConfig(level=logging.INFO)

client = nextcord.Client()

class Gender(nextcord.ui.Select):
    options = [
        nextcord.SelectOption(label='Genderqueer', description='', emoji='🏳️‍🌈'),
        nextcord.SelectOption(label='Genderfluid', description='', emoji='🌊'),
        nextcord.SelectOption(label='Agender', description='', emoji='🟩'),
        nextcord.SelectOption(label='Transgender', description='', emoji='🏳️‍⚧️'),
        nextcord.SelectOption(label='Bigender', description='', emoji='💜'),
        nextcord.SelectOption(label='Transmasc', description='', emoji='👦'),
        nextcord.SelectOption(label='Transfem', description='', emoji='👧'),
        nextcord.SelectOption(label='Non-binary', description='', emoji='🧑'),
        nextcord.SelectOption(label='Cis Girl', description='', emoji='👧'),
        nextcord.SelectOption(label='Cis Boy', description='', emoji='👦'),
        nextcord.SelectOption(label='Intersex', description='', emoji='⭕'),
        nextcord.SelectOption(label='Pangender', description='', emoji='♾️'),
        nextcord.SelectOption(label='Questioning Gender', description='', emoji='❓')
    ]
    def __init__(self):
        super().__init__(placeholder='Set your gender...', min_values=0, max_values=len(self.options), options=self.options)

    async def callback(self, interaction):
        # Removing all roles
        for label in self.options:
            role = nextcord.utils.get(interaction.guild.roles, name=label.label)
            if role != None:
                await interaction.user.remove_roles(role)

        for label in self.values:
            # Add roles, if not found, then create it
            role = nextcord.utils.get(interaction.guild.roles, name=label)
            if role == None:
                role = await interaction.guild.create_role(name=label)
            await interaction.user.add_roles(role)
        await interaction.response.send_message(f"Your gender(s) has been set to {self.values}", ephemeral=True)

class Sexuality(nextcord.ui.Select):
    options = [
        nextcord.SelectOption(label='Gay', emoji='👬'),
        nextcord.SelectOption(label='Lesbian', emoji='👭'),
        nextcord.SelectOption(label='Bisexual', emoji='💜'),
        nextcord.SelectOption(label='Pansexual', emoji='♾️'),
        nextcord.SelectOption(label='Polyamorous', emoji='➕'),
        nextcord.SelectOption(label='Asexual', emoji='🧄'),
        nextcord.SelectOption(label='Aegosexual', emoji='🧄'),
        nextcord.SelectOption(label='Abrosexual', emoji='🌊'),
        nextcord.SelectOption(label='Straight', emoji='👫'),
        nextcord.SelectOption(label='Polysexual', emoji='♾️'),
        nextcord.SelectOption(label='Queer', emoji='🌈'),
        nextcord.SelectOption(label='Omnisexual', emoji='♾️'),
        nextcord.SelectOption(label='Questioning', emoji='❓')
    ]
    def __init__(self):
        super().__init__(placeholder='Set your sexuality...', min_values=0, max_values=len(self.options), options=self.options)

    async def callback(self, interaction):
        # Removing all roles
        for label in self.options:
            role = nextcord.utils.get(interaction.guild.roles, name=label.label)
            if role != None:
                await interaction.user.remove_roles(role)

        for label in self.values:
            # Add roles, if not found, then create it
            role = nextcord.utils.get(interaction.guild.roles, name=label)
            if role == None:
                role = await interaction.guild.create_role(name=label)
            await interaction.user.add_roles(role)
        await interaction.response.send_message(f"Your sexuality has been set to {self.values}", ephemeral=True)

class Romantic(nextcord.ui.Select):
    options = [
        nextcord.SelectOption(label='Gay (homoromantic)', emoji='👬'),
        nextcord.SelectOption(label='Lesbian (homoromantic)', emoji='👭'),
        nextcord.SelectOption(label='Biromantic', emoji='💜'),
        nextcord.SelectOption(label='Panromantic', emoji='♾️'),
        nextcord.SelectOption(label='Aromantic', emoji='🧄'),
        nextcord.SelectOption(label='Straight', emoji='👫'),
        nextcord.SelectOption(label='Polyromantic', emoji='♾️'),
        nextcord.SelectOption(label='Queer', emoji='🌈'),
        nextcord.SelectOption(label='Omniromantic', emoji='♾️'),
        nextcord.SelectOption(label='Questioning', emoji='❓')
    ]
    def __init__(self):
        super().__init__(placeholder='Set your romantic attraction...', min_values=0, max_values=len(self.options), options=self.options)

    async def callback(self, interaction):
        # Removing all roles
        for label in self.options:
            role = nextcord.utils.get(interaction.guild.roles, name=label.label)
            if role != None:
                await interaction.user.remove_roles(role)

        for label in self.values:
            # Add roles, if not found, then create it
            role = nextcord.utils.get(interaction.guild.roles, name=label)
            if role == None:
                role = await interaction.guild.create_role(name=label)
            await interaction.user.add_roles(role)
        await interaction.response.send_message(f"Your romantic attraction has been set to {self.values}", ephemeral=True)

class DropdownView(nextcord.ui.View):
    def __init__(self):
        super().__init__()
        self.add_item(Gender())
        self.add_item(Sexuality())
        self.add_item(Romantic())

@client.slash_command(guild_ids=ids)
async def roles(interaction):
    view = DropdownView()
    await interaction.response.send_message('Pick your roles:', view=view)

client.run(token)
