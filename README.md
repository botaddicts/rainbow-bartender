# Rainbow Bartender Bot
This bot is a custom bot for [The (Actually) Queer Pub](https://discord.gg/ft3zMQjwyJ).

## Roadmap
- [x] Role selector and creator for various genders, sexual and romantic attractions. 
- [ ] Trying various pronouns

## Technicals
I struggled a bit with the new nextcord package, but I really wanted to use the dropdown menus for this. 

Feel free to use my code as a base to create your own menus or role-selection bots.

## Setting up
1. Clone the repository and install requirements

```bash
git clone https://codeberg.org/botaddicts/rainbow-bartender.git 
cd rainbow-bartender/
pip install -r requirements.txt
```

2. Create a new file called "config.py" in that directory.

```bash
nano config.py
```

3. Paste the config (token + id of the server)

```python
token = "your token here"
ids = [123456789]
```

4. Running the server

```bash
python3 main.py
```
